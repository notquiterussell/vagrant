#!/bin/bash

MAVEN_VERSION=3.3.9

echo "Provisioning virtual machine..."

apt-get update -qq

echo "Installing Git"
add-apt-repository ppa:git-core/ppa -y > /dev/null
apt-get install git -y > /dev/null

if  ./apache-maven-$MAVEN_VERSION/bin/mvn -version 2>&1 | grep 'Maven'; then
   	echo "skip maven installation"
else
	echo "Maven installation"
	wget http://www.mirrorservice.org/sites/ftp.apache.org/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz
	tar xvf apache-maven-$MAVEN_VERSION-bin.tar.gz
	rm apache-maven-$MAVEN_VERSION-bin.tar.gz
fi

if which pip >/dev/null; then
	echo "Skip pip installation"
else
	echo "Installing Python tools..."
	apt-get install python-pip --yes
fi

echo "Installing AWS Tools..."
pip install awscli --upgrade

if which docker >/dev/null; then
	echo "Skip docker installation"
else
	wget -qO- https://get.docker.com/ | sh
	usermod -aG docker $(whoami)
	pip install docker-compose
fi

if which sambda >/dev/null; then
	echo "Skip samba installation"
else
	apt-get install samba --yes
fi 

cp /vagrant/provision/smb.conf /etc/samba/smb.conf
service samba restart

apt-get autoremove --yes

if grep -q provision/environment.sh "/home/vagrant/.bashrc"; then
	echo "Skipping shell config."
else
	echo "Setting up shell..."
	echo "source /vagrant/provision/environment.sh" >> /home/vagrant/.bashrc
fi
    
if which java >/dev/null; then
	echo "Skip java installation"
else
	echo "java 8 installation"
	apt-get install --yes python-software-properties
	
	echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
	echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections
	echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee /etc/apt/sources.list.d/webupd8team-java.list
	echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
	apt-get update
	apt-get install --yes oracle-java8-installer
	
	yes "" | apt-get -f install
fi
