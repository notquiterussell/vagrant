###########
# Maven
###########
export MAVEN_HOME=~/apache-maven-3.3.9
export PATH=$MAVEN_HOME/bin:$PATH
export PATH=$PATH:~/bin

#########################
# Git
#########################
# Show present working directory and Git branch at prompt
# source: http://www.developerzen.com/2011/01/10/show-the-current-git-branch-in-your-command-prompt/
function parse_git_branch () {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status

# prompt config:
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "

# DIR
LS_COLORS=$LS_COLORS:'di=0;36:' ; export LS_COLORS
