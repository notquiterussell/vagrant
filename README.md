# Environment Setup #

## Introduction ##

## Prerequisites ##
* Install Virtualbox for your operating system https://www.virtualbox.org/wiki/Downloads
* Install Vagrant for your operating system: https://www.vagrantup.com/downloads.html
* If you are on Windows install git https://git-scm.com/download/win. When prompted choose the option "Use Git and optional Unix tools from the Windows Command Prompt"

## First-time ##
To install and run the system for the first time:
* Create ssh keys in your Windows host by opening a command prompt and running ssh-keygen. There's a useful tutorial here: https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
* Run the batch file start.bat
* Run the batch file provision.bat
* Run the batch file vagrant-share.bat

## Each time ##
Whenever you restart Windows:
* start.bat (to start Vagrant)
* login.bat - login, forwarding your ssh keys from Windows to Linux
* stop.bat (when you're done)

## Whenever a change is announced upstream ##
* Run the batch file provision.bat
